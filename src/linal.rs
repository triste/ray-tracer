use std::ops::{Add, Sub, Mul, Div, Neg};

/* TODO: should we use this crate? Seems bloated... */
//pub use num_traits::{Float, Zero};

pub trait Scalar<Rhs=Self>
    : Add<Output = Rhs>
    + Sub<Output = Rhs>
    + Mul<Output = Rhs>
    + Div<Output = Rhs>
    + Copy
{
    fn sqrt(self) -> Self;
    fn zero() -> Self;
}

macro_rules! impl_scalar {
    ($($type:ty),+) => {
        $(
            impl Scalar for $type {
                fn sqrt(self) -> Self { self.sqrt() }
                fn zero() -> $type { 0.0 }
            }
        )+
    }
}

impl_scalar!(f32, f64);

macro_rules! make_vec {
    ($vec:ident [$($axis:ident),+]) => {

        #[derive(Clone, Copy, Debug, Eq, PartialEq, Default)]
        pub struct $vec<T> {
            $(pub $axis: T),+
        }

        impl<T> $vec<T> {
            pub fn new($($axis: T),+) -> Self {
                Self { $($axis),+ }
            }
        }

        impl<T: Copy> $vec<T> {
            #[inline]
            pub fn map<O>(&self, f: fn(T) -> O) -> $vec<O> {
                $vec::<O> {
                    $($axis: f(self.$axis)),+
                }
            }
        }

        macro_rules! impl_binop {
            ($trait:ident, $function:ident, $ltype:ty, $rtype:ty) => {
                impl<'a, 'b, T> $trait<$rtype> for $ltype
                    where T: $trait<Output = T> + Copy
                {
                    type Output = $vec<T>;

                    #[inline]
                    fn $function(self, other: $rtype) -> Self::Output {
                        Self::Output {
                            $($axis: self.$axis.$function(other.$axis)),+
                        }
                    }
                }
            }
        }


        macro_rules! impl_binop_with_scalar {
            ($trait:ident, $function:ident, $ltype:ty, $rtype:ty) => {
                impl<'a, T> $trait<T> for $ltype
                    where T: $trait<Output = T> + Copy
                {
                    type Output = $vec<T>;

                    #[inline]
                    fn $function(self, scalar: $rtype) -> Self::Output {
                        Self::Output {
                            $($axis: self.$axis.$function(scalar)),+
                        }
                    }
                }
            }
        }

        macro_rules! impl_unop {
            ($trait:ident, $function:ident, $type:ty) => {
                impl<'a, T> $trait for $type
                    where T: $trait<Output = T> + Copy
                {
                    type Output = $vec<T>;

                    #[inline]
                    fn $function(self) -> Self::Output {
                        Self::Output {
                            $($axis: self.$axis.$function()),+
                        }
                    }
                }
            }
        }

        macro_rules! impl_binop_helper {
            ($trait:ident, $function:ident) => {
                impl_binop!($trait, $function, $vec<T>, $vec<T>);
                impl_binop!($trait, $function, &'a $vec<T>, $vec<T>);
                impl_binop!($trait, $function, &'a $vec<T>, &'b $vec<T>);
                impl_binop!($trait, $function, $vec<T>, &'b $vec<T>);
                impl_binop_with_scalar!($trait, $function, $vec<T>, T);
                impl_binop_with_scalar!($trait, $function, &'a $vec<T>, T);
            }
        }

        macro_rules! impl_unop_helper {
            ($trait:ident, $function:ident) => {
                impl_unop!($trait, $function, $vec<T>);
                impl_unop!($trait, $function, &'a $vec<T>);
            }
        }

        impl_binop_helper!(Div, div);
        impl_binop_helper!(Add, add);
        impl_binop_helper!(Sub, sub);
        impl_binop_helper!(Mul, mul);

        impl_unop_helper!(Neg, neg);


        impl<T: Scalar> $vec<T> {
            pub fn dot(&self, other: &Self) -> T {
                $(self.$axis * other.$axis +)+ T::zero()
            }
            pub fn lenght(&self) -> T {
                self.dot(self).sqrt()
            }
            pub fn unit(&self) -> Self {
                self / self.lenght()
            }
            pub fn zero() -> Self {
                Self { $($axis: T::zero()),+ }
            }
        }
    }
}

make_vec!(Vector3 [x, y, z]);

impl<T: Scalar> Vector3<T> {
    pub fn cross(&self, other: &Self) -> Self {
        Self {
            x: self.y * other.z - self.z * other.y,
            y: self.z * other.x - self.x * other.z,
            z: self.x * other.y - self.y * other.x,
        }
    }
}

make_vec!(Vector4 [x, y, z, w]);
