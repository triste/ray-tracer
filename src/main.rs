use std::convert::TryInto;
use std::fs::File;
use std::rc::Rc;

use rand::{Rng, thread_rng};
use rand::distributions::{Distribution, Standard};

mod linal;

use linal::{Vector3, Vector4};

type Vec3 = Vector3<f32>;

fn is_near_zero(v: Vec3) -> bool {
    let s = 1e-8;
    v.x.abs() < s && v.y.abs() < s && v.z.abs() < s
}

impl<T> Distribution<Vector3<T>> for Standard 
    where Standard: Distribution<T>
{
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> Vector3<T> {
        Vector3::<T> {
            x: rng.gen(),
            y: rng.gen(),
            z: rng.gen(),
        }
    }
}

fn random_in_unit_sphere() -> Vec3 {
    loop {
        let mut rng = thread_rng();
        let p = rng.gen::<Vec3>() * 2.0 - 1.0;
        if p.dot(&p) < 1.0 {
            return p;
        }
    }
}

fn random_in_unit_hemisphere(normal: &Vec3) -> Vec3 {
    let in_unit_sphere = random_in_unit_sphere();
    if in_unit_sphere.dot(normal) > 0.0 {
        in_unit_sphere
    } else {
        - in_unit_sphere
    }
}

impl Vec3 {
    fn reflect(&self, normal: &Vec3) -> Vec3 {
        self - normal * self.dot(normal) * 2.0
    }
    fn refract(&self, normal: &Vec3, refract_ratio: f32) -> Vec3 {
        let cosin = normal.dot(&- self);
        //let sin = (1.0 - cosin * cosin).sqrt();

        let r_perp = (normal * cosin + self) * refract_ratio;

        //let r_perp = (normal * cosin + self).unit() * refract_ratio * sin;

        let r_par = normal * (-((1.0 - r_perp.dot(&r_perp)).sqrt()));
        
        r_perp + r_par
    }
}

struct Metal {
    albedo: Color,
    fuzz: f32,
}

struct Dielectric {
    ir: f32,
    filter: f32,
}

fn reflectance(cosine: f32, ref_idx: f32) -> f32 {
    let r = (1.0 - ref_idx) / (1.0 + ref_idx);
    let r = r * r;

    r + (1.0 - r) * (1.0 - cosine).powi(5)
}

impl Material for Dielectric {
    fn scatter(&self, ray_in: &Ray, hitinfo: &HitInfo) -> Option<(Ray, Color)> {
        let attenuation = Color::new(1.0, 1.0, 1.0) * self.filter;
        let refraction_ratio = if hitinfo.is_front_face {
            1.0 / self.ir
        } else {
            self.ir
        };

        let unit_direction = ray_in.direction.unit();

        let cosin = - unit_direction.dot(&hitinfo.normal);
        let cosin = if cosin > 1.0 {
            1.0
        } else {
            cosin
        };
        let sin = (1.0 - cosin * cosin).sqrt();

        let is_cannot_refract = refraction_ratio * sin > 1.0;

        let mut rng = thread_rng();
        let mut rand_float = rng.gen_range(0.0..1.0);
        let mut r  = reflectance(cosin, refraction_ratio);
        let is_reflectance = r > rand_float;

        let direction = if is_cannot_refract || is_reflectance {
            unit_direction.reflect(&hitinfo.normal)
        } else {
            unit_direction.refract(&hitinfo.normal, refraction_ratio)
        };

        let scattered = Ray::new(hitinfo.p, direction);
        Some((scattered , attenuation))

    }
}

impl Material for Metal {
    fn scatter(&self, ray_in: &Ray, hitinfo: &HitInfo) -> Option<(Ray, Color)> {
        let reflected = ray_in.direction.reflect(&hitinfo.normal);
        let scattered = Ray::new(hitinfo.p, reflected + random_in_unit_sphere() * self.fuzz);
        let attenuation = self.albedo;

        if scattered.direction.dot(&hitinfo.normal) > 0.0 {
            Some((scattered, attenuation))
        } else {
            None
        }
    }
}

type Pixel = Vector4<u8>;

impl<T: Copy> Vector4<T> {
    fn r(&self) -> T { self.x }
    fn g(&self) -> T { self.y }
    fn b(&self) -> T { self.z }
    fn a(&self) -> T { self.w }
}

struct Image {
    pixels: Vec<Pixel>,
    width: u16,
    height: u16,
}

impl Image {
    fn new(width: u16, height: u16, bg: Pixel) -> Self {
        let lenght = (width as usize * height as usize * 4).try_into().unwrap();
        let pixels = vec![bg; lenght];
        Self { width, height, pixels }
    }
    fn write<W: std::io::Write>(self, writer: &mut W) {
        let width = self.width.to_le_bytes(); 
        let height = self.height.to_le_bytes();

        let header = vec![
            0, // id length
            0, // color map type (0: no color map)
            2, // image type (2: uncompressed true-color image)
            0, 0, 0, 0, 0, // color map specifiaction

            // image specification
            0, 0, // x-origin
            0, 0, // y-origin
            width[0], width[1],
            height[0], height[1],
            32, // pixel depth
            0, // image descriptor
        ];
        let pixels: Vec<_> = self.pixels.iter().flat_map(|p| vec![p.b(), p.g(), p.r(), p.a()]).collect();

        writer.write_all(&header).unwrap();
        writer.write_all(&pixels).unwrap();
    }
    fn set_pixel(&mut self, x: u16, y: u16, color: Color, samples: i32) {
        let scale = 1.0 / samples as f32;
        // gamma correction 1/2: x^1/2 === sqrt x
        let rgb = (color * scale).map(|m| (m.sqrt().clamp(0.0, 0.999) * 256.0) as u8);
        let coord = y as usize * self.width as usize + x as usize;
        self.pixels[coord] = Pixel::new(rgb.r(), rgb.g(), rgb.b(), 255);
    }
}

type Color = Vector3<f32>;
impl<T: Copy> Vector3<T> {
    fn r(&self) -> T { self.x }
    fn g(&self) -> T { self.y }
    fn b(&self) -> T { self.z }
}

struct Ray {
    origin: Vec3,
    direction: Vec3,
}

impl Ray {
    fn new(origin: Vec3, direction: Vec3) -> Self {
        Self { origin, direction }
    }
    fn at(&self, t: f32) -> Vec3 {
        self.origin + self.direction * t
    }
}

fn ray_color<T: Hittable>(ray: &Ray, world: &T, depth: i32) -> Color {
    if depth <= 0 {
        return Color::zero();
    }
    let rec = world.hit(ray, 0.001, std::f32::INFINITY);
    if let Some(rec) = rec {
        let ret = rec.mat_ref.scatter(ray, &rec);
        if let Some((scattered, attenuation)) = ret {
            return attenuation * ray_color(&scattered, world, depth - 1);
        } else {
            return Color::zero();
        }
    }
    let unit_direction = ray.direction.unit();
    let t = 0.5 * (unit_direction.y + 1.0);
    (Color::zero() + 1.0) * (1.0 - t) + Color::new(0.5, 0.7, 1.0) * t
}

#[derive(Clone)]
struct HitInfo {
    p: Vec3,
    normal: Vec3,
    t: f32,
    is_front_face: bool,
    mat_ref: Rc<dyn Material>,
}

impl Default for HitInfo {
    fn default() -> Self {
        Self {
            p: Default::default(),
            normal: Default::default(),
            t: Default::default(),
            is_front_face: Default::default(),
            mat_ref: Rc::new(Lambertian { albedo: Default::default() }),
        }
    }
}

#[derive(Copy, Clone, Default)]
struct Lambertian {
    albedo: Color,
}

impl Material for Lambertian {
    fn scatter(&self, _: &Ray, hitinfo: &HitInfo) -> Option<(Ray, Color)> {
        let mut scatter_direction = hitinfo.normal + random_in_unit_hemisphere(&hitinfo.normal).unit();
        if is_near_zero(scatter_direction) {
            scatter_direction = hitinfo.normal;
        }
        let scattered = Ray::new(hitinfo.p, scatter_direction);
        Some((scattered, self.albedo))
    }
}

impl HitInfo {
    fn set_face_normal(&mut self, ray: &Ray, outward_normal: &Vec3) {
        self.is_front_face = ray.direction.dot(outward_normal) < 0.0;
        self.normal = if self.is_front_face {
            *outward_normal
        } else {
            - outward_normal
        };
    }
}

trait Material {
    fn scatter(&self, ray_in: &Ray, hitinfo: &HitInfo) -> Option<(Ray, Color)>;
}

trait Hittable {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitInfo>;
}

struct Sphere {
    center: Vec3,
    radius: f32,
    mat_ref: Rc<dyn Material>,
}

impl Hittable for Sphere {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitInfo> {
        let oc = ray.origin - self.center;
        let a = ray.direction.dot(&ray.direction);
        let half_b = oc.dot(&ray.direction);
        let c = oc.dot(&oc) - self.radius * self.radius;
        let discriminant = half_b * half_b - a * c;
        if discriminant < 0.0 {
            return None;
        }

        let squarted_discriminant = discriminant.sqrt();
        let mut root = (- half_b - squarted_discriminant) / a;
        if root < t_min || root > t_max {
            root = (- half_b + squarted_discriminant) / a;
            if root < t_min || root > t_max {
                return None;
            }
        }
        let mut hit_info = HitInfo {
            p: ray.at(root),
            normal: (ray.at(root) - self.center) / self.radius,
            t: root,
            is_front_face: false,
            mat_ref: self.mat_ref.clone(),
        };


        let outward_normal = (ray.at(root) - self.center) / self.radius;
        hit_info.set_face_normal(ray, &outward_normal);

        Some(hit_info)
    }
}

impl Hittable for Vec<Rc<dyn Hittable>> {
    fn hit(&self, ray: &Ray, t_min: f32, t_max: f32) -> Option<HitInfo> {
        let mut is_hit_any = false;
        let mut closest_so_far = t_max;
        let mut record = Default::default();

        for obj in self {
            let temp_hitinfo = obj.hit(ray, t_min, closest_so_far);
            if let Some(hitinfo) = temp_hitinfo {
                is_hit_any = true;
                closest_so_far = hitinfo.t;
                record = hitinfo;
            }
        }
        if is_hit_any == true {
            Some(record)
        } else {
            None
        }
    }
}

struct Camera {
    origin: Vec3,
    lower_left_corner: Vec3,
    horizontal: Vec3,
    vertical: Vec3,

    u: Vec3,
    v: Vec3,
    w: Vec3,
    lens_radius: f32,
}

impl Camera {
    fn new(look_from: Vec3, look_at: Vec3, up: Vec3, vertical_fov: f32, aspect_ratio: f32, aperture: f32, focus_distance: f32) -> Self {
        let theta = vertical_fov.to_radians();
        let height = (theta / 2.0).tan();
        let viewport_height = height * 2.0;
        let viewport_width = aspect_ratio * viewport_height;

        /* Right-hand coord system */
        let w = (look_from - look_at).unit();
        let u = up.cross(&w).unit();
        let v = w.cross(&u).unit();

        let origin = look_from;
        let horizontal = u * viewport_width * focus_distance;
        let vertical = v * viewport_height * focus_distance;
        let lower_left_corner = origin
            - horizontal / 2.0
            - vertical / 2.0
            - w * focus_distance;

        let lens_radius = aperture / 2.0;
        Self { origin, lower_left_corner, horizontal, vertical, u, v, w, lens_radius }
    }
    fn get_ray(&self, u: f32, v: f32) -> Ray {
        let rd = random_in_unit_disk() * self.lens_radius;
        let offset = (self.u * rd.x) + (self.v * rd.y);
        Ray::new(self.origin + offset,
                 self.lower_left_corner +
                 self.horizontal * u
                 + self.vertical * v
                 - self.origin - offset)
    }
}

fn random_in_unit_disk() -> Vec3 {
    let random = || thread_rng().gen_range(-1.0..1.0);

    loop {
        let p = Vec3::new(random(), random(), 0.0);
        if p.dot(&p) >= 1.0 {
            continue;
        }
        return p;
    }
}

fn main() {
    let aspect_ratio = 16.0 / 9.0;
    let width = 1080;
    let height = (width as f32 / aspect_ratio) as u16;
    let samples = 50;
    let max_depth = 20;


    let bg = Pixel::new(0, 0, 0, 255);

    let mut image = Image::new(width, height, bg);

    let glass = Rc::new(Dielectric { ir: 1.5, filter: 1.0 });
    let ground_mat = Lambertian { albedo: Color::new(0.5, 0.5, 0.5) };
    let red = Rc::new(Lambertian { albedo: Color::new(0.8, 0.2, 0.2) });
    let blue = Rc::new(Lambertian { albedo: Color::new(0.2, 0.2, 0.8) });
    let green = Rc::new(Lambertian { albedo: Color::new(0.2, 0.8, 0.2) });

    let world: Vec<Rc<dyn Hittable>> = vec![
        Rc::new(Sphere {
            center: Vec3 { x: 0.0, y: -100.5, z: -1.0 },
            radius: 100.0,
            mat_ref: Rc::new(ground_mat),
        }),

        Rc::new(Sphere {
            center: Vec3 { x: -1.0, y: 0.0, z: -1.0 },
            radius: 0.5,
            mat_ref: glass.clone(),
        }),
        Rc::new(Sphere {
            center: Vec3 { x: -1.0, y: 0.0, z: -1.0 },
            radius: -0.45,
            mat_ref: glass.clone(),
        }),
        Rc::new(Sphere {
            center: Vec3 { x: 0.0, y: 0.0, z: -1.0 },
            radius: 0.5,
            mat_ref: blue,
        }),
        Rc::new(Sphere {
            center: Vec3 { x: 1.0, y: 0.0, z: -1.0 },
            radius: 0.5,
            mat_ref: red,
        }),
        /*
        Rc::new(Sphere {
            center: Vec3 { x: -2.3, y: 0.0, z: -4.0 },
            radius: 0.5,
            mat_ref: Rc::new(other_mat1),
        }),
        Rc::new(Sphere {
            center: Vec3 { x: 2.3, y: 1.0, z: -4.0 },
            radius: 0.5,
            mat_ref: Rc::new(other_mat2),
        }),
        Rc::new(Sphere {
            center: Vec3 { x: 0.0, y: 1.0, z: -4.0 },
            radius: 0.5,
            mat_ref: Rc::new(other_mat3),
        }),
        Rc::new(Sphere {
            center: Vec3 { x: 0.0, y: 0.1, z: -1.0 },
            radius: 0.5,
            mat_ref: center_mat.clone(),
        }),
        Rc::new(Sphere {
            center: Vec3 { x: 0.0, y: 0.1, z: -1.0 },
            radius: -0.4,
            mat_ref: center_mat.clone(),
        }),
        */
    ];

    let look_from = Vec3::new(3.0, 3.0, 2.0);
    let look_at = Vec3::new(0.0, 0.0, -1.0);
    let vup = Vec3::new(0.0, 1.0, 0.0);
    let dist_to_focus = (look_from - look_at).lenght();
    let camera = Camera::new(look_from, look_at, vup, 20.0, aspect_ratio, 2.0, dist_to_focus);

    let mut rng = thread_rng();

    for j in (0..image.height).rev() {
        println!("line: {}", j);
        for i in 0..image.width {
            let mut color = Color::zero();
            for _ in 0..samples {
                let u = (i as f32 + rng.gen::<f32>()) / (image.width - 1) as f32;
                let v = (j as f32 + rng.gen::<f32>()) / (image.height - 1) as f32;
                let ray = camera.get_ray(u, v);
                color = color + ray_color(&ray, &world, max_depth);
            }

            image.set_pixel(i, j, color, samples);
        }
    }

    let mut buffer = File::create("balls.tga").unwrap();
    image.write(&mut buffer);
}
